//------------------------------------------------------------------------------------
//                           I N C L U D E
//------------------------------------------------------------------------------------

#include "car.h"
#include "track.h"
#include "misc.h"
#include <stdlib.h>
#include <math.h>
#include <string>
#include <mysql++.h>

//--------------------------------------------------------------------------------------
//                              KLASSE ROBOT 04
//--------------------------------------------------------------------------------------

class Robot04 : public Driver
{

public:

//______________________________KONSTRUKTOR_____________________________________________
    Robot04()
    {
        m_sName = "Robot04";
        m_sAuthor = "Gruppe 02";
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        m_sModel3D = "futura";

        Anpassung_Attribute();
        //Aufruf der Funktion, welche alle Attribute auf den Anfangswert 0.0 setzt

    }

//--------------------------------------------------------------------------------------
//                                 ATTRIBUTE
//--------------------------------------------------------------------------------------

private:

    double alpha;               //--------------------------------------------------
    double vc;                  //
    double fuel_amount;         // Attribute, welche am Ende ausgegeben werden
    int repair_amount;          //
    int request_pit;            //--------------------------------------------------

    double zielabstand_l;
    double lenkungsverstaerkung;
    double daempfungsverstaerkung;
    double reibungskoeffizient;
    double bremsbeschleunigung;
    double bremsbeschleunigung_kurve;
    double delta_reifengeschwindigkeit;
    double c;   //Variable welche die maximale Kurven_Geschwindigkeit ausreizt

//--------------------------------------------------------------------------------------
//                                 KONSTANTEN
//--------------------------------------------------------------------------------------
#define TANK_KRIT 20
#define SCHADEN_KRIT 20000

    /**
    *   Attribute auf den Anfangwert 0.0 setzen
    *   Parameter: keine
    *   Rückgabe: keine
    */
    void Anpassung_Attribute()
    {
        alpha = 0.0;
        vc = 0.0;

        fuel_amount = 0.0;
        repair_amount = 0.0;
        request_pit = 0.0;

        zielabstand_l = 0.0;
        lenkungsverstaerkung = 0.0;
        daempfungsverstaerkung = 0.0;
        reibungskoeffizient = 0.0;
        bremsbeschleunigung = 0.0;
        bremsbeschleunigung_kurve = 0.0;
        delta_reifengeschwindigkeit = 0.0;
        c = 0.0;
    }


    /**
    *   Funktion: Anpassung der Parameter an die aktuelle zu befahrenen Strecke
    *   Parameter: keine
    *   Rückgabe: keine
    */
    void Anpassung_Strecke()
    {
        struct track_desc Trackinfo = get_track_description(); //Information der Strecke einlesen

        if(!strcmp(Trackinfo.sName,"adelaide.trk")) //wird ausgeführt wenn man sich auf der Strecke Adelaide befindet
        {
            zielabstand_l = 5;
            lenkungsverstaerkung = 0.502;
            daempfungsverstaerkung = 1.15;
            reibungskoeffizient = 0.988;
            bremsbeschleunigung = -36.5;
            bremsbeschleunigung_kurve = -27.1;
            delta_reifengeschwindigkeit = 4;
            c = 11.3;
        }

        if(!strcmp(Trackinfo.sName,"brazil.trk")) //wird ausgeführt wenn man sich auf der Strecke Brazil befindet
        {
            zielabstand_l = 3.7;
            lenkungsverstaerkung = 0.2;
            daempfungsverstaerkung = 1.2;
            reibungskoeffizient = 0.945;
            bremsbeschleunigung = -36.3;
            bremsbeschleunigung_kurve = -35.5;
            delta_reifengeschwindigkeit = 3.5;
            c = 10.02;

        }

        if(!strcmp(Trackinfo.sName,"hock.trk")) //wird ausgeführt wenn man sich auf der Strecke Hockenheim befindet
        {
            zielabstand_l = 5;
            lenkungsverstaerkung = 0.502;
            daempfungsverstaerkung = 1.15;
            reibungskoeffizient = 0.988;
            bremsbeschleunigung = -36.5;
            bremsbeschleunigung_kurve = -27.1;
            delta_reifengeschwindigkeit = 4;
            c = 11.35;
        }

        if(!strcmp(Trackinfo.sName,"imola.trk")) //wird ausgeführt wenn man sich auf der Strecke Imola befindet
        {
            zielabstand_l = 8;
            lenkungsverstaerkung = 0.495;
            daempfungsverstaerkung = 1.49;
            reibungskoeffizient = 0.9795;
            bremsbeschleunigung = -39.5;
            bremsbeschleunigung_kurve = -29;
            delta_reifengeschwindigkeit = 4.0;
            c = 15.9;
        }

        if(!strcmp(Trackinfo.sName,"silverst.trk")) //wird ausgeführt wenn man sich auf der Strecke Silverstone befindet
        {
            zielabstand_l = 4;
            lenkungsverstaerkung = 0.45;
            daempfungsverstaerkung = 1.48;
            reibungskoeffizient = 0.95;
            bremsbeschleunigung = -35.5;
            bremsbeschleunigung_kurve = -27.3;
            delta_reifengeschwindigkeit = 4;
            c = 16.5;
        }
    }


    /**
    *   Funktion: Gibt die maximale moegliche Kurven_Geschwindigkeit an
    *   Parameter: in - double radius:               Einheit: Fuß       radius der Kurve, deren Geschwindigkeit zu berechnen ist.
    *   Rückgabewert: maximale Kurvengeschwindigkeit Einheit: Fuß/m
    */
    double Kurven_Geschwindigkeit(double radius) const
    {
        double naechste_geschwindigkeit;

        naechste_geschwindigkeit = sqrt(fabs(radius) * g * reibungskoeffizient); //berechnet maximale Kurvengeschwindigkeit
        return (naechste_geschwindigkeit + c);
    }


    /**
    *   Funktion: Gibt den minimalen moeglichen Abbremsweg an
    *   Parameter: in - double akt_v:       Einheit: Fuß/s    aktuelle Geschwindigkeit.
    *              in - double naechstes_v: Einheit: Fuß/s    gewuenschte Geschwindigkeit am Ende des aktuellen Abschnittes.
    *              in - double brems_a:     Einheit: Fuß/s²   negative Beschleunigung beim Bremsen.
    *   Rueckgabewert: minimaler Bremsweg.  Einheit: Fuß
    */
    double Min_Abbremsweg(double akt_v, double naechstes_v, double brems_a) const
    {
        double delta_v = naechstes_v - akt_v;  //Differenz zwischen gewuenschter und aktueller Geschwindigkeit

        if(delta_v >= 0.0)  // wenn die Zielgeschwindigkeit größer ist, keine Bremswegberechnung
        {
            return 0;
        }

        return(delta_v * (akt_v + 0.5 * delta_v) / brems_a); // Berechnung des minimalen Bremsweges
    }

    /**
    *Funktion: Gibt die Geschwindigkeit eines Abschnittes an
    *Parameter: in - double radius:        Einheit: Fuß     radius des Abschnittes
    *           in - double zielabstand:   Einheit: Fuß     gewuenschter Abstand zur linken Wand
    *Rückgabewert:Zielgeschwindigkeit des zu berechnenden Abschnittes   Einheit: Fuß
    */
    double Berechn_Geschw_Abschnitt(double radius,double zielabstand) const
    {
        double berechn_geschw;

        if(radius > 0)  //Linkskurve
        {
            berechn_geschw = Kurven_Geschwindigkeit(radius + zielabstand);
        }
        else if(radius < 0) //Rechtskurve
        {
            berechn_geschw = Kurven_Geschwindigkeit(radius - zielabstand); // zielabstand ist neg., da radius neg. ist
        }
        else    //Gerade
        {
            berechn_geschw = 200; // maximale Beschleunigung
        }

        return berechn_geschw;
    }

    /**
    *   Funktion: Gibt den Drift-Winkel Alpha an
    *   Parameter: in - situation &s:                          aktuelle Situation des Robots
    *              in - double zielabstand:   Einheit: Fuß     gewuenschter Abstand zur linken Wand
    *   Rueckgabewert: Drift-Winkel Alpha in Abhaengigkeit von dem gewuenschten zielabstand
    */
    double Berechn_Alpha(situation &s, double radius) const
    {
        struct track_desc Trackinfo = get_track_description(); //Streckeninformationen einlesen
        double Spurbreite = Trackinfo.width; // Spurbreite definieren, durch die Funktion aus dem Framework
        double zielabstand_r = Spurbreite - zielabstand_l;  //Zielabstand zur rechten Wand
        // Berechnung des Drift-Winkel Alpha angepasst an Links oder Rechtskurve
        if (radius > 0)
        {
            return (lenkungsverstaerkung * (s.to_lft - zielabstand_l)/Spurbreite - daempfungsverstaerkung*(s.vn/s.v));
        }
        else
        {
            return (lenkungsverstaerkung * (s.to_lft - zielabstand_r)/Spurbreite - daempfungsverstaerkung*(s.vn/s.v));
        }

    }


    /** Funktion: Berechnet anhand der Situation s die Geschwindigkeit und den Lenkwinkel
    *   Parameter: in - situation &s:   aktuelle Situation des Robots
    *   Rückgabe: Keine
    */
    void Fahren(situation &s)
    {
        double naechste_geschwindigkeit = 0;
        double aktuelle_geschwindigkeit = 0;
        double kurven_laenge = 0;
        double bis_ende_kurve = 0;

        naechste_geschwindigkeit = Berechn_Geschw_Abschnitt(s.nex_rad, zielabstand_l);
        //Berechnung der Geschwindigkeit des naechsten Abschnittes

        if (s.cur_rad == 0.0) //Gerade
        {

            if(s.to_end > Min_Abbremsweg(s.v, naechste_geschwindigkeit, bremsbeschleunigung))
                // wenn das Ende des Abschnittes noch groesser als der minimale Abbremsweg ist
            {
                vc = 500; // Vollgas
                alpha = Berechn_Alpha(s, s.nex_rad);
                //  zielabstand auf der Geraden wird auf den zielabstand der naechsten Kurve gesetzt
            }

            else if(s.to_end < Min_Abbremsweg(s.v, naechste_geschwindigkeit, bremsbeschleunigung))
                // wenn das Ende des Abschnittes geringer als der minimale Abbremsweg ist
            {
                vc = s.v + bremsbeschleunigung; // bremsen
            }
        }

        aktuelle_geschwindigkeit = Berechn_Geschw_Abschnitt(s.cur_rad, zielabstand_l);
        //Zielgeschwindigkeit des aktuellen Abschnittes

        kurven_laenge = s.cur_len*(fabs(s.cur_rad) + zielabstand_l);// Laenge der Kurve und die Angabe bis zum Ende der
        bis_ende_kurve = s.to_end*(fabs(s.cur_rad) + zielabstand_l);//  Kurve wird auf den gewuenschten zielabstand angepasst


        if ((s.cur_rad != 0.0) && (aktuelle_geschwindigkeit > naechste_geschwindigkeit ))
        {
            if(bis_ende_kurve > Min_Abbremsweg(s.v, naechste_geschwindigkeit, bremsbeschleunigung_kurve))
            {
                vc = (aktuelle_geschwindigkeit) / cos(alpha);
                alpha = Berechn_Alpha(s, s.cur_rad);
            }
            else // wenn die Strecke bis zum Ende des Abschnittes kleiner als der minimale Abbremsweg ist
            {
                alpha = Berechn_Alpha(s, s.nex_rad);

                if (s.v > (naechste_geschwindigkeit + delta_reifengeschwindigkeit))
                    // + delta_reifengeschwindigkeit dient dazu, dass s.v nicht kleiner als naechste geschwindigkeit wird
                {
                    vc = s.v - delta_reifengeschwindigkeit;
                    //wird so lange ausgefuehrt bis s.v kleiner als naechste_geschwindigkeit + delta_reifengeschwindigkeit ist
                }
                else
                {
                    vc = naechste_geschwindigkeit/ cos(alpha);
                }

            }
        }
        if ((s.cur_rad != 0.0) && (aktuelle_geschwindigkeit < naechste_geschwindigkeit ))
            //Kurve und aktuelle Geschwindigkeit ist kleiner als die nächste
        {

            if (bis_ende_kurve > kurven_laenge/10) //wenn das Ende der Kurve noch lang ist
            {
                alpha = Berechn_Alpha(s, s.cur_rad);
                vc = aktuelle_geschwindigkeit/ cos(alpha);
            }

            else if (s.nex_rad == 0) // wenn naechste Strecke eine Gerade ist
            {
                alpha = Berechn_Alpha(s, s.after_rad);
                vc = naechste_geschwindigkeit/cos(alpha);
            }
            else // wenn naechste Strecke eine Kurve ist
            {
                alpha = Berechn_Alpha(s, s.nex_rad);
                vc = naechste_geschwindigkeit/cos(alpha);

            }

        }
    Kollisionsvermeidung(s,alpha);
    }

    /** Funktion: Schreibt verschiedene Renndaten in eine Mysql Datanbank
    *   Parameter: in - situation &s:   aktuelle Situation des Robots
    *   Rückgabe: Keine
    */


    void Mysql(situation &s )
    {

        mysqlpp::Connection conn( false );

        if( !conn.connect( "rars", "localhost", "rars", "r2a0c1e4") )
        {
            return;
        }

        mysqlpp::Query query = conn.query("insert into tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES");
        query << "(" << mysqlpp::quote << m_sName << ","
        << s.start_time << ","
        << s.time_count << ","
        << s.laps_done << ","
        << s.v << ","
        << s.fuel << ","
        << s.damage << ","
        << mysqlpp::quote << get_track_description().sName
        << ");";
        query.execute();

    }

    /** Funktion: Kollisionsvermeidung
    *   Parameter: in - situation &s: aktuelle Situation des Robots
    *              in - double alpha: aktueller Driftwinkel Alpha
    *   Rückgabe: keine
    */

    void Kollisionsvermeidung(situation& s, double alpha)
    {
        struct track_desc Trackinfo = get_track_description(); //Streckeninformationen einlesen
        double Spurbreite = Trackinfo.width; // Spurbreite definieren, durch die Funktion aus dem Framework
        double zielabstand_r = Spurbreite - zielabstand_l;  //Zielabstand zur rechten Wand

        for (int i=0; i < 4; i++)               //Prüft die nächsten 4 Robots
        {
            double abst_seitl=s.nearby[i].rel_x; //relativer seitlicher Abstand
            double abst_vorn=s.nearby[i].rel_y;  //relativer Abstand nach vorne

            if (s.cur_rad >0)                   //Prüfen ob wir uns in einer Kurve befinden, dann kein Überholen
            {
                abst_vorn=1.1*CARLEN;
                abst_seitl=1.1*CARWID;
            }
            else if (s.cur_rad<0)
            {
                abst_vorn=1.1*CARLEN;
                abst_seitl=-1.1*CARWID;
            }


            if (abst_vorn< 1.1*CARLEN )          //Wird nur auf der Gereade ausgeführt, wenn Robot zu nahe
             {
                 if (abst_seitl<0 &&(zielabstand_r + abs(abst_seitl))>2*CARWID ) //Robot befindet sich links von uns und der Abstsand rechts reicht zum Überholen
                 {
                     alpha=abst_seitl-1.1*CARWID;
                 }
                 if (abst_seitl>0 && (zielabstand_l+abst_seitl)>2*CARWID) //Robot befindet sich rechts von uns und der Abstand links reicht zum Überholen
                 {
                     alpha=abst_seitl+1.1*CARWID;
                 }
             }
             else
             {
                 abst_vorn = 1.1*CARLEN;
             }

        }
    }

    /** Funktion: entscheidet anhand der aktuellen Tankfüllung und des aktuellen Schadens ob ein Boxenstopp ausgeführt werden soll
    *   Parameter: in - situation &s - aktuelle Situation des Robots
    *   Rückgabe:       1:                     Boxenstopp findet statt
    *                   0:                     Boxenstopp findet nicht statt
    */
   bool Boxenstopp(situation &s)
   {
      if ((s.fuel < TANK_KRIT || s.damage > SCHADEN_KRIT) && s.laps_to_go != 1)
      {
         request_pit = 1;                //Boxenstopp anfordern
         repair_amount = s.damage;       //Menge an Schadenspunkten die repariert werden soll
         fuel_amount = MAX_FUEL;         //Menge an Benzin die nachgetankt werden soll
         return 1;
      }
      else
         return 0;
   }


//--------------------------------------------------------------------------------------
//                           HAUPT-FAHRFUNKTION
//--------------------------------------------------------------------------------------
public:

    con_vec drive(situation &s)
    {
        con_vec result;

        if(s.starting)
        {
            result.fuel_amount = MAX_FUEL;//Setzt den Tank auf volle Ladung, wird nur beim Start aufgerufen
            Anpassung_Strecke(); // Parameter werden an die aktuelle beFahrene Strecke angepasst
        }
        if(stuck( s.backward, s.v, s.vn, s.to_lft, s.to_rgt, &result.alpha, &result.vc))
            //holt den Robot zurück auf die Strecke (Funktion des Frameworks)
        {
            return result;
        }

        Fahren(s);
        Mysql(s);
        Boxenstopp(s);

        result.request_pit = request_pit;
        result.fuel_amount = fuel_amount;
        result.repair_amount = repair_amount;

        result.vc =vc;
        result.alpha = alpha;
        return result;
    }
};


/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot04Instance()
{
    return new Robot04();
}
