/**
 * Roboter 02 05 blue red
 *
 * @author    Teresa Diener
 * @version   Übungsrobot
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <stdlib.h>
#include <math.h>
#include <string>
#include "car.h"
using namespace std;

//--------------------------------------------------------------------------
//                           Class Robot05
//--------------------------------------------------------------------------

class Robot05 : public Driver
{
public:
    // Konstruktor
    Robot05()
    {
        // Der Name des Robots
        m_sName = "Robot05";
        // Namen der Autoren
        m_sAuthor = "Teresa Diener";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oRED;
        //Name der Bitmapdatei
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }
protected:

//Benötigte Konstanten
static const double KURVEN_BESCH = 1.0;     // seitliche Beschleunigung in den Kurven
static const double BREMS_BESCH = -33.0;   // Beschleunigung beim abremsen auf einer Geraden
static const double BREMS_SLIP  = 6.5;     // Radbewegung beim Bremsen auf einer Geraden
static const double BREMS_BESCH_KURV = -27.0;   // Beschleunigung beim abbremsen in einer Kurve
static const double BREMS_SLIP_KURV = 3.5;    // Radbewegung beim Bremsen in einer Kurve
static const double TEMPO = 1.02;    // eine Konstante um die Geschwindigkeit in den Kurven zu überprüfen

static const double ABSTAND = 10.0;     //direkter Abstand vom Kurveninnenradius
static const double ABSTAND2 = 20.0; //direkte Angaben zum Kurveneintritt
static const double KURVEN_ENDE = 4.0;       // Nahe am Ende einer Kurve
static const double KURVEN_NEIG = .33;       // Neigung des Weges vor der Kurve
static const double BIG_SLIP = 9.0;       //bestimmt die lenkung des Servos
static const double LENK_ANDER = 1.0;      //Aenderung des Servos
static const double DAMP_ANDER = 1.2;      //Daempfung der Lenkung
static const double DELTA_LANE = 2.5;   // bei Kollisionsgefahr weg Ändern

//Benötigte Variablen
double breite;                 // Streckenbreite, Einheit: Fuß
double linie;           // Abstand von der linken Wand, Einheit: Fuß
double linie0;          // Wert von Linie während dem ersten Teil einer Geraden
double lenkung;
double winkel; //Der Lenkwinkel des Robots
double gesch_akt;     //aktuelle Segmentsgeschwindigkeit, Einheit: Fuß/Sekunden
double gesch_nach; //Benötigte Segmentsgeschwindigkeit im nächsten Segment/am Ende des aktuellen Segments, Einheit Fuß/Sekunden
double linie_inc;  //Anpassung der linie zum Vorbeifahren
double  gesch;             // Geschwindigkeit des Robot
double ende;                // Abstand zum Ende des aktuellen Segments

//Benötigte ausgelagerte Funktionen

    /* Funktion: Berechnet die maximale Kurvengeschwindigkeit
        Parameter: in Radius Einheit: Fuß
        Rückgabe: maximale Kurvengeschwindigkeit Einheit: Fuß/Sekunde
    */
    double Kurven_Gesch(double radius)
    {
        if(radius <= 0)  //Verhindert, dass die Funktion mit einem negativen Argument oder Null aufgerufen wird.
            return 0;
        return sqrt(radius * 32.2 * KURVEN_BESCH);
    }


    /* Funktion: Berechnet die kritische Distanz um den Robot abzubremsen
        Parameter: in aktuelle Geschwindigkeit v0 Einheit: Fuß/Sek
                in benötigte Geschwindigkeit v1 Einheit: Fuß/Sek
                in Beschleunigung a Einheit: Fuß/Sekunde²
        Rückgabe: out Kritischer Abstand Einheit:Fuß
    */
    double KritAbst(double v0, double v1, double a)
    {
        double dv;

        dv = v1 - v0;
        if(dv > 0.0 || a < 0)          //Test ob v0 größer v1 und a negativ, dann keine berechnung
            return(0.0);
        return (v0 + .5 * dv) * dv / a;
  }


    /* Funktion: Berechnet die Steuerung in den Kurven
        Parameter: die aktuelle situation
        Rückgabe: keine
    */
void Kurven_Steuer (situation& s)
{
        breite = s.to_lft + s.to_rgt;  // berechnet die Streckenbreite

        if(s.starting)                // wird nur einmal wahr
        linie= linie0 = s.to_lft;

        static int radius_vor = 0;       // 0, 1, oder -1 um die Art des vorherigen Segmentes zu speichern

        int radius = s.cur_rad;
        switch( radius > 0 ? : radius < 0 ?: radius )  //Kurvensteuerung
        {
            case 1: //nach Links fahren
                linie = ABSTAND;
                radius_vor = 1;
                break;
            case 2: //nach Rechts fahren
                linie = breite -ABSTAND;
                radius_vor = -1;
                break;
            default:             //ansonsten geradeausfahren
                if(radius_vor!=0)                 // Falls der Robot gerade eine Kurve gefahren hat
                    {
                        linie = s.to_lft;          // linie auf die aktuelle Position setzten
                        if(linie < .5 * breite)     // eventuell die Linie etwas nach rechts korrigieren
                        {
                            linie += ABSTAND2;
                        }
                        linie0 = linie;             // neuen Wert in die Variable speichern
                        radius_vor = 0;
                    }
                /* Im Endbereich einer Geraden wird die Linie von dem gespeicherten Wert linieO zu Beginn auf
                    ABSTAND2 am Ende der Geraden gebracht. KURVEN_NEIG sind die Änderungen der linie pro Änderungen
                    der Funktion
                */
               if(s.to_end < (linie0 - ABSTAND2) / KURVEN_NEIG)
                {
                    linie = ABSTAND2 + KURVEN_NEIG * s.to_end;
                }

            }
}
    /* Funktion: Berechnet die Steuerung im folgenden Streckenabschnitt
        Parameter: die aktuelle situation
        Rückgabe: keine
    */
void Lenkung(situation& s)
{
        //Berechnet die benötigte Geschwindigkeit
        if(s.cur_rad == 0.0)   //aktuell auf einer Geraden
        {
            lenkung = 0.0;
           int radiusn = s.nex_rad;
            switch( radiusn > 0 ? : radiusn < 0 ?: radiusn )
            {
                case 1:
                    gesch_akt = Kurven_Gesch(s.nex_rad + ABSTAND);
                    break;
                case 2:
                    gesch_akt = Kurven_Gesch(-s.nex_rad + ABSTAND);
                    break;
                default:
                    gesch_akt = 250.0;
                    break;
            }
        }
        else                     // aktuell in einer Kurve
        {
            if(s.nex_rad == 0.0)
            {
                gesch_nach = 250.0;
            }
            else
            {
                gesch_nach = Kurven_Gesch(fabs(s.nex_rad) + ABSTAND);
            }

            gesch_akt = Kurven_Gesch(fabs(s.cur_rad) + ABSTAND + fabs(linie_inc));

            lenkung = (s.v*s.v/(gesch_akt*gesch_akt)) * atan(BIG_SLIP / gesch_akt);
            if(s.cur_rad < 0.0)     //Negativer Kurvenradius -> Rechtskurve ->negative lenkung
            {
                lenkung = -lenkung;
            }
        }

        winkel = LENK_ANDER * (s.to_lft - linie)/breite - DAMP_ANDER * s.vn/s.v + lenkung;//Der Servo um den Robot korrekt zu lenken


        //Berechnet die benötigte Beschleunigung
        if(s.cur_rad == 0.0)              // Aktuell auf einer Geraden
        {
            if(s.to_end > KritAbst(s.v, gesch_akt, BREMS_BESCH)) //Prüfen ob der kritische Abstand
                gesch = s.v + 50.0;                             // maximale Beschleunigung
            else
            {
                if(s.v > TEMPO * gesch_akt)        // Robot zu schnell
                    gesch = s.v - BREMS_SLIP;         // stark bremsen
                else if(s.v < gesch_akt/TEMPO)     // Robot zu langsam
                    gesch = 1.1 * gesch_akt;          // stark beschleunigen
                else                                  // Nahe an der benötigten Geschwindigkeit
                    gesch = .5 * (s.v + gesch_akt);   // Geschwindigkeit langsam anpassen
            }
        }
        else       // aktuell in einer Kurve
        {
            if(s.cur_rad > 0.0)  //Abstand zum Ende der Kurve berechnen
                ende = s.to_end * (s.cur_rad + ABSTAND);
            else
                ende = -s.to_end * (s.cur_rad - ABSTAND);

            if(ende <= KritAbst(s.v, gesch_nach, BREMS_BESCH_KURV)) //Abbremsen falls das nächste Segment eine kleinere Kurve ist
                gesch = s.v - BREMS_SLIP_KURV;

            else if(ende/breite < KURVEN_ENDE && gesch_nach > gesch_akt) //Beschleunigen falls eine Gerade oder größere Kurve folgt
                gesch = .5 * (s.v + gesch_nach)/cos(winkel);
            else                                                          //Ansonsten Geschwindigkeit berechnen
                gesch = .5 * (s.v + gesch_akt)/cos(winkel);
        }
}
/* Funktion: Kollisionsvermeidung
    Parameter: in aktuelle situation
    Rückgabe: keine
*/

void Anti_Kollision(situation& s)
{
    double x, y, vx, vy, dot, vsqr, c_time, y_close, x_close;
    int kount;     //zählt die Robots die eine Kollisionsgefahr bringen
    kount = 0;
    for(int i=0;i<3;i++) if (s.nearby[i].who<16)  // wenn kollisionsgefahr besteht
    {
      y=s.nearby[i].rel_y;
      x=s.nearby[i].rel_x;
      vx=s.nearby[i].rel_xdot;
      vy=s.nearby[i].rel_ydot;

      dot = x * vx + y * vy;
      if(dot > -0.1)
        continue;
      vsqr = vx*vx + vy*vy;

      c_time = -dot / vsqr;
      if(c_time > 3.0)          //bei über 3 Sekunden ignorieren
        continue;

  x_close = x + c_time * vx;
      y_close = y + c_time * vy;

  if(x_close * x < 0.0 && y < 1.1 * CARLEN)
      {
        c_time = (fabs(x) - CARWID) / fabs(vx);
        x_close = x + c_time * vx;
        y_close = y + c_time * vy;
      }

      if(fabs(x_close) > 2 * CARWID || fabs(y_close) > 1.25 * CARLEN)
        continue;

      ++kount;
      if(kount > 1 || c_time < .85)  // mehr als ein Robot oder innerhalb von 0.85 sekunden zu Kollision
        gesch = s.v - BREMS_SLIP_KURV;

      if(s.cur_rad > 0.0)
        if(x_close < 0.0 || s.to_lft < ABSTAND)
          linie_inc += DELTA_LANE;
        else
          linie_inc -= DELTA_LANE;
      else if(s.cur_rad < 0.0)
        if(x_close > 0.0 || s.to_rgt < ABSTAND)
          linie_inc -= DELTA_LANE;
        else
          linie_inc += DELTA_LANE;
      else if(x_close < 0.0)
        linie_inc += DELTA_LANE;
      else
        linie_inc -= DELTA_LANE;
      if(linie_inc > .25 * breite)
        linie_inc = .25 * breite;
      else if(linie_inc < -.25 * breite)
        linie_inc = -.25 * breite;
    }

    if(!kount)
      {if(linie_inc > .1)
        {
            linie_inc -= .5*DELTA_LANE;
        }

      else if(linie_inc < -.001)
        {
          linie_inc += .5*DELTA_LANE;
        }
      }




}


//Hauptfunktion, Logik des Robots
    con_vec drive(situation& s)
    {

        con_vec result = CON_VEC_EMPTY; //Das zurückgegebene Ergebnis

        if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha,&result.vc)) //Kollisionsvermeidung, Funktion der Hauptdatei
        {
            return result;
        }

        Kurven_Steuer(s);
        Lenkung(s);
        Anti_Kollision(s);

        if( s.fuel<10.0 || s.damage>25000 ) //Falls zu wenig Sprit oder zu viel Schaden: Boxenstopp
        {
            result.request_pit   = 1;
            result.repair_amount = s.damage;
            result.fuel_amount = MAX_FUEL;
        }


        result.vc = gesch;   result.alpha = winkel - LENK_ANDER * linie_inc / breite;

        return result;  //Ende der con_vec Funktion
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot05Instance()
{
    return new Robot05();
}
